module gitlab.com/spdf/elektron

go 1.12

require (
	github.com/fatih/color v1.7.0
	github.com/gogo/protobuf v1.3.0 // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/golang/protobuf v1.3.2
	github.com/mash/gokmeans v0.0.0-20170215130432-ea22cff45f59
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/mesos/mesos-go v0.0.10
	github.com/montanaflynn/stats v0.5.0
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/samuel/go-zookeeper v0.0.0-20190923202752-2cc03de413da // indirect
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20190927123631-a832865fa7ad
)
